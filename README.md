# campsite-reservation-manager

This project is the implementation of the code challenge bellow:

>_An underwater volcano formed a new small island in the Pacific Ocean last month._ 
>
>_All the conditions on the island seems perfect and it was decided to open it up for the general public to experience the pristine uncharted territory._
>
>_The island is big enough to host a single campsite so everybody is very excited to visit._ 
>
>_In order to regulate the number of people on the island, it was decided to come up with an online web application to manage the reservations._ 
>
>_You are responsible for design and development of a REST API service that will manage the campsite reservations._

**Constraints:**

* The campsite will be free for all.
* The campsite can be reserved for max 3 days.
* The campsite can be reserved minimum 1 day(s) ahead of arrival and up to 1 month in - advance.
* Reservations can be cancelled anytime.
* For sake of simplicity assume the check-in & check-out time is 12:00 AM

**System requirements:**

* The users will need to find out when the campsite is available. So the system should expose an API to provide information of theavailability of the campsite for a given date range with the default being 1 month.
* Provide an end point for reserving the campsite. The user will provide his/her email & full name at the time of reserving the campsite along with intended arrival date and departure date. Return a unique booking identifier back to the caller if the reservation is successful.
* The unique booking identifier can be used to modify or cancel the reservation later on. Provide appropriate end point(s) to allow modification/cancellation of an existing reservation.
* Due to the popularity of the island, there is a high likelihood of multiple users attempting to reserve the campsite for the same/overlapping date(s). Demonstrate with appropriate test cases that the system can gracefully handle concurrent requests to reserve the campsite.
* Provide appropriate error messages to the caller to indicate the error cases.
* In general, the system should be able to handle large volume of requests for getting the campsite availability.
* There are no restrictions on how reservations are stored as as long as system constraints are not violated.

## Implementation notes

### Stack:
* [Spring Boot](https://spring.io/projects/spring-boot)
* [H2 Database](http://www.h2database.com/html/main.html)
* [Mockito](https://site.mockito.org/)
* [ModelMapper](http://modelmapper.org/)
* [JaCoCo](https://www.eclemma.org/jacoco/)
* [Maven](https://maven.apache.org/)

### Installation

The project requires Java SDK 8 to run.

Clone this project and fire up maven to install dependencies and start the server.  
  
```sh
$ mkdir tiagomelo_evaluation
$ cd tiagomelo_evaluation
$ git clone https://tiagoharris@bitbucket.org/tiagoharris/campsite-reservation-manager.git
$ cd campsite-reservation-manager
$ mvn spring-boot:run
```

### Endpoints
I'll use [cURL](https://curl.haxx.se/) in the examples. Another good option is [Postman](https://www.getpostman.com/).

#### Campsite availability
URL: 


`/api/reservation/availability`



Method:

`GET`



URL Params: 


`fromDate=[yyyy-MM-dd]`


`toDate=[yyyy-MM-dd]`


if not informed, the date range will be defaulted for 1 month from current date.



Sample call:


```sh
curl -v "http://localhost:8080/api/reservation/availability?fromDate=2019-02-01&toDate=2019-02-03""
```


if available, will return:


```json
{
  "fromDate": "2019-02-01",
  "toDate": "2019-02-03",
  "available": true
}
```


otherwise, will return a list of next available dates:


```json
{
  "fromDate": "2019-02-01",
  "toDate": "2019-02-03",
  "available": false,
  "availableDates": [
    "2019-02-04",
    "2019-02-05",
    "2019-02-06",
    "2019-02-07",
    "2019-02-08",
    "2019-02-09",
    "2019-02-10",
    "2019-02-11",
    "2019-02-12",
    "2019-02-13",
    "2019-02-14",
    "2019-02-15",
    "2019-02-16",
    "2019-02-17",
    "2019-02-18",
    "2019-02-19",
    "2019-02-20",
    "2019-02-21",
    "2019-02-22",
    "2019-02-23",
    "2019-02-24",
    "2019-02-25",
    "2019-02-26",
    "2019-02-27",
    "2019-02-28"
  ]
}
```


#### Create reservation
URL:


`/api/reservation/`


Method:


`POST`


Data Params:


```json
{
  "fromDate": "2019-02-01",
  "toDate": "2019-02-03",
  "name": "Tiago",
  "email": "tiago@email.com"
}
```


Sample call:


```sh
curl -v -H "Content-Type: application/json" "http://localhost:8080/api/reservation" -d '{"fromDate":"2019-02-01", "toDate":"2019-02-03", "name":"Tiago", "email":"tiago@email.com"}'
```


if available, will return the reservation data, where UUID is its unique identifier:


```json
{
  "fromDate": "2019-02-01",
  "toDate": "2019-02-03",
  "uuid": "d2bfb79a-78b9-4228-a6cb-0b393ad37835"
}
```


otherwise, will return error with HTTP status 409:


```json
{
  "errorCode": "Conflict",
  "errorMessage": "The requested period between 2019-02-01 and 2019-02-03 is not available for reservation"
}
```


#### Update reservation
URL:


`/api/reservation/`


Method:


`PUT`


URL Params:


`uuid=[RESERVATION_UUID]`



Data Params:


```json
{
  "fromDate": "2019-02-03",
  "toDate": "2019-02-06",
  "uuid": "d2bfb79a-78b9-4228-a6cb-0b393ad37835"
}
```


Sample call:


```sh
curl -v -H "Content-Type: application/json" -X PUT "http://localhost:8080/api/reservation/d2bfb79a-78b9-4228-a6cb-0b393ad37835" -d '{"fromDate":"2019-02-03", "toDate":"2019-02-06", "uuid":"d2bfb79a-78b9-4228-a6cb-0b393ad37835"}'
```


if available, will return the updated reservation data:


```json
{
  "fromDate": "2019-02-03",
  "toDate": "2019-02-06",
  "uuid": "d2bfb79a-78b9-4228-a6cb-0b393ad37835"
}
```


otherwise, will return error with HTTP status 409:


```json
{
  "errorCode": "Conflict",
  "errorMessage": "The requested period between 2019-02-03 and 2019-02-06 is not available for reservation"
}
```


if any reservation matches the informed uuid, will return error with HTTP status 404:


```json
{
  "errorCode": "Not Found",
  "errorMessage": "Reservation not found with uuid: 'd2bfb79a-78b9-4228-a6cb-0b393ad37836'"
}
```


#### Cancel reservation
URL: 


`/api/reservation/`


Method:


`DELETE`


URL Params:


`uuid=[RESERVATION_UUID]`


Sample call:


```sh
curl -v -X DELETE "http://localhost:8080/api/reservation/d2bfb79a-78b9-4228-a6cb-0b393ad37835"'
```


if success, will only return HTTP status 200.



if any reservation matches the informed uuid, will return error with HTTP status 404:


```json
{
  "errorCode": "Not Found",
  "errorMessage": "Reservation not found with uuid: 'd2bfb79a-78b9-4228-a6cb-0b393ad37836'"
}
```


### Tests
This project has 100% of test coverage.


Fire up maven to run all unit tests and generate JaCoCo report:


```sh
$ mvn test && mvn jacoco:report
```


The report will be available at:


```sh
<PROJECT_DIRECTORY>/target/site/jacoco/index.html
```

