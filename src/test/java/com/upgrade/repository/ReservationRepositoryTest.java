package com.upgrade.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
@SqlGroup({
  @Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:insertReservations.sql"),
  @Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:deleteReservations.sql") })
public class ReservationRepositoryTest {

  @Autowired
  ReservationRepository reservationRepository;
  
  @Test
  public void whenTheresReservationsFromThreeDifferentMonths_thenReturnReservationsBetweenDesiredMonths() {
    List<Object[]> reservations = reservationRepository.findAllReservationsBetweenMonths(2, 3);
    
    assertEquals(10, reservations.size());
  }
  
  @Test
  public void whenTheresAReservationThatOverlapsTheDesiredReservation_thenReturnReservation() {
    LocalDate desiredFromDate = LocalDate.of(2019, Month.JANUARY, 13);
    LocalDate desiredToDate = LocalDate.of(2019, Month.JANUARY, 15);
    
    assertNotNull(reservationRepository.findOverlappingReservation(desiredFromDate, desiredToDate));
  }
  
  @Test
  public void whenTheresNoReservationThatOverlapsTheDesiredReservation_thenReturnNull() {
    LocalDate desiredFromDate = LocalDate.of(2020, Month.JANUARY, 13);
    LocalDate desiredToDate = LocalDate.of(2020, Month.JANUARY, 15);
    
    assertNull(reservationRepository.findOverlappingReservation(desiredFromDate, desiredToDate));
  }
  
  @Test
  public void whenTheresAReservationWithCorrespondingUUID_thenReturnReservation() {
    UUID desiredUuid = UUID.fromString("dc4b2292-5796-45b3-b030-b9c4a0324e1b");
    
    assertNotNull(reservationRepository.findByUuid(desiredUuid));
  }
}
