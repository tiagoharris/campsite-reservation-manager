package com.upgrade.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.upgrade.entity.Guest;
import com.upgrade.entity.Reservation;
import com.upgrade.exception.ResourceNotFoundException;
import com.upgrade.exception.UnavailableDateRangeException;
import com.upgrade.repository.GuestRepository;
import com.upgrade.repository.ReservationRepository;
import com.upgrade.service.ReservationService;
import com.upgrade.util.DateUtil;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { ReservationServiceImpl.class, DateUtil.class })
public class ReservationServiceImplTest {

  @Autowired
  private ReservationService service;

  @MockBean
  private ReservationRepository reservationRepository;

  @MockBean
  private GuestRepository guestRepository;

  private static final UUID VALID_UUID = UUID.fromString("dc4b2292-5796-45b3-b030-b9c4a0324e1b");

  @Before
  public void setUp() {
    Mockito.when(
        reservationRepository.findOverlappingReservation(LocalDate.parse("2019-01-04"), LocalDate.parse("2019-01-06")))
        .thenReturn(buildReservation());

    Mockito.when(reservationRepository.findAllReservationsBetweenMonths(anyInt(), anyInt()))
        .thenReturn(buildReservations());

    Mockito.when(reservationRepository.findByUuid(VALID_UUID)).thenReturn(buildOptionalReservation());
  }

  @Test
  public void WhenDesiredDateRangeOverlapsExistentReservation_thenReturnAListOfAvailableDates() {
    LocalDate fromDate = LocalDate.parse("2019-01-10");
    LocalDate toDate = LocalDate.parse("2019-01-13");

    List<LocalDate> availableDates = buildExpectedAvailableDates();

    assertEquals(availableDates, service.findNextAvailableDates(fromDate, toDate));
  }

  @Test
  public void WhenDesiredDateRangeIsAMonthPeriodAndItOverlapsExistentReservation_thenReturnAListOfAvailableDates() {
    LocalDate fromDate = LocalDate.parse("2019-01-10");
    LocalDate toDate = LocalDate.parse("2019-02-10");

    List<LocalDate> availableDates = buildExpectedAvailableDatesWhenDateRangeIsAMonthPeriod();

    assertEquals(availableDates, service.findNextAvailableDates(fromDate, toDate));
  }

  @Test
  public void WhenDesiredDateRangeDoesNotOverlapsAnyExistentReservation_thenReturnNull() {
    LocalDate fromDate = LocalDate.parse("2019-01-04");
    LocalDate toDate = LocalDate.parse("2019-01-06");

    assertNull(service.findNextAvailableDates(fromDate, toDate));
  }

  @Test
  public void WhenSavingANewReservationThatDoesNotOverlapsAnyExistentReservation_thenReturnNewReservation() {
    Reservation newReservation = new Reservation();

    newReservation.setFromDate(LocalDate.parse("2019-01-01"));
    newReservation.setToDate(LocalDate.parse("2019-01-02"));

    Guest newGuest = new Guest();
    newGuest.setId(1);
    newReservation.setGuest(newGuest);

    Reservation savedReservation = service.save(newReservation);

    assertEquals(newReservation, savedReservation);
    assertEquals(newGuest.getId(), savedReservation.getGuest().getId());
  }

  @Test(expected = UnavailableDateRangeException.class)
  public void WhenSavingANewReservationThatOverlapsAnyExistentReservation_thenRaisesUnavailableDateRangeException() {
    Reservation newReservation = new Reservation();

    newReservation.setFromDate(LocalDate.parse("2019-01-04"));
    newReservation.setToDate(LocalDate.parse("2019-01-06"));

    service.save(newReservation);
  }

  @Test
  public void WhenSavingAnExistingReservationThatDoesNotOverlapsAnyExistentReservation_thenReturnNewReservation() {
    Reservation existingReservation = new Reservation();

    existingReservation.setFromDate(LocalDate.parse("2019-01-01"));
    existingReservation.setToDate(LocalDate.parse("2019-01-02"));
    existingReservation.setUuid(UUID.randomUUID());

    Reservation savedReservation = service.save(existingReservation);

    assertEquals(existingReservation, savedReservation);
  }

  @Test(expected = UnavailableDateRangeException.class)
  public void WhenSavingAnExistingReservationThatOverlapsExistentReservation_thenReturnError() {
    Reservation existingReservation = new Reservation();

    existingReservation.setFromDate(LocalDate.parse("2019-01-04"));
    existingReservation.setToDate(LocalDate.parse("2019-01-06"));
    existingReservation.setUuid(UUID.randomUUID());

    Reservation savedReservation = service.save(existingReservation);

    assertEquals(existingReservation, savedReservation);
  }

  @Test
  public void WhenSavingAnExistingReservationThatOverlapsItsOwnDates_thenUpdateReservation() {
    Reservation existingReservation = new Reservation();

    existingReservation.setFromDate(LocalDate.parse("2019-01-04"));
    existingReservation.setToDate(LocalDate.parse("2019-01-06"));
    existingReservation.setUuid(UUID.randomUUID());
    existingReservation.setId(666);

    Reservation savedReservation = service.save(existingReservation);

    assertEquals(existingReservation, savedReservation);
  }

  @Test
  public void WhenFindingReservationByExistingUuid_thenReturnReservation() {
    assertNotNull(service.findByUUID(VALID_UUID));
  }

  @Test(expected = ResourceNotFoundException.class)
  public void WhenFindingReservationByNonExistingUuid_thenRaisesResourceNotFoundException() {
    service.findByUUID(null);
  }

  @Test
  public void WhenDeletingReservationByExistingUuid_thenShouldDeleteBothReservationAndGuest() {
    Reservation reservation = buildReservation();

    service.delete(VALID_UUID);

    verify(reservationRepository).delete(reservation);
    verify(guestRepository).delete(reservation.getGuest());
  }

  @Test(expected = ResourceNotFoundException.class)
  public void WhenDeletingReservationByNonExistingUuid_thenRaisesResourceNotFoundException() {
    service.delete(null);
  }

  private List<Object[]> buildReservations() {
    List<Object[]> reservations = new ArrayList<Object[]>();

    reservations.add(new Object[] { LocalDate.parse("2019-01-01"), LocalDate.parse("2019-01-03") });
    reservations.add(new Object[] { LocalDate.parse("2019-01-07"), LocalDate.parse("2019-01-08") });
    reservations.add(new Object[] { LocalDate.parse("2019-01-11"), LocalDate.parse("2019-01-14") });
    reservations.add(new Object[] { LocalDate.parse("2019-02-04"), LocalDate.parse("2019-02-07") });
    reservations.add(new Object[] { LocalDate.parse("2019-02-08"), LocalDate.parse("2019-02-09") });
    reservations.add(new Object[] { LocalDate.parse("2019-02-15"), LocalDate.parse("2019-02-18") });

    return reservations;
  }

  private List<LocalDate> buildExpectedAvailableDates() {
    List<LocalDate> availableDates = new ArrayList<LocalDate>();

    availableDates.add(LocalDate.parse("2019-01-04"));
    availableDates.add(LocalDate.parse("2019-01-05"));
    availableDates.add(LocalDate.parse("2019-01-06"));
    availableDates.add(LocalDate.parse("2019-01-09"));
    availableDates.add(LocalDate.parse("2019-01-10"));
    availableDates.add(LocalDate.parse("2019-01-15"));
    availableDates.add(LocalDate.parse("2019-01-16"));
    availableDates.add(LocalDate.parse("2019-01-17"));
    availableDates.add(LocalDate.parse("2019-01-18"));
    availableDates.add(LocalDate.parse("2019-01-19"));
    availableDates.add(LocalDate.parse("2019-01-20"));
    availableDates.add(LocalDate.parse("2019-01-21"));
    availableDates.add(LocalDate.parse("2019-01-22"));
    availableDates.add(LocalDate.parse("2019-01-23"));
    availableDates.add(LocalDate.parse("2019-01-24"));
    availableDates.add(LocalDate.parse("2019-01-25"));
    availableDates.add(LocalDate.parse("2019-01-26"));
    availableDates.add(LocalDate.parse("2019-01-27"));
    availableDates.add(LocalDate.parse("2019-01-28"));
    availableDates.add(LocalDate.parse("2019-01-29"));
    availableDates.add(LocalDate.parse("2019-01-30"));
    availableDates.add(LocalDate.parse("2019-01-31"));

    return availableDates;
  }

  private List<LocalDate> buildExpectedAvailableDatesWhenDateRangeIsAMonthPeriod() {
    List<LocalDate> availableDates = new ArrayList<LocalDate>();

    availableDates.addAll(buildExpectedAvailableDates());

    List<LocalDate> availableDatesFromNextMonth = new ArrayList<LocalDate>();

    availableDatesFromNextMonth.add(LocalDate.parse("2019-02-01"));
    availableDatesFromNextMonth.add(LocalDate.parse("2019-02-02"));
    availableDatesFromNextMonth.add(LocalDate.parse("2019-02-03"));
    availableDatesFromNextMonth.add(LocalDate.parse("2019-02-10"));
    availableDatesFromNextMonth.add(LocalDate.parse("2019-02-11"));
    availableDatesFromNextMonth.add(LocalDate.parse("2019-02-12"));
    availableDatesFromNextMonth.add(LocalDate.parse("2019-02-13"));
    availableDatesFromNextMonth.add(LocalDate.parse("2019-02-14"));
    availableDatesFromNextMonth.add(LocalDate.parse("2019-02-19"));
    availableDatesFromNextMonth.add(LocalDate.parse("2019-02-20"));
    availableDatesFromNextMonth.add(LocalDate.parse("2019-02-21"));
    availableDatesFromNextMonth.add(LocalDate.parse("2019-02-22"));
    availableDatesFromNextMonth.add(LocalDate.parse("2019-02-23"));
    availableDatesFromNextMonth.add(LocalDate.parse("2019-02-24"));
    availableDatesFromNextMonth.add(LocalDate.parse("2019-02-25"));
    availableDatesFromNextMonth.add(LocalDate.parse("2019-02-26"));
    availableDatesFromNextMonth.add(LocalDate.parse("2019-02-27"));
    availableDatesFromNextMonth.add(LocalDate.parse("2019-02-28"));

    availableDates.addAll(availableDatesFromNextMonth);

    return availableDates;
  }

  private Reservation buildReservation() {
    Reservation existingReservation = new Reservation();
    existingReservation.setId(666);

    return existingReservation;
  }

  private Optional<Reservation> buildOptionalReservation() {
    return Optional.of(buildReservation());
  }
}
