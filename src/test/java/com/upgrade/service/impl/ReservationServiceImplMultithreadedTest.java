package com.upgrade.service.impl;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.upgrade.entity.Guest;
import com.upgrade.entity.Reservation;
import com.upgrade.repository.ReservationRepository;
import com.upgrade.service.ReservationService;

/**
 * This class represents a test case of multiple threads attempting to 
 * make the same reservation.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class ReservationServiceImplMultithreadedTest {

  @Autowired
  ReservationService service;

  @Autowired
  ReservationRepository reservationRepository;

  AtomicInteger threadExecutionCount = new AtomicInteger();

  @Test
  public void test() throws InterruptedException, ExecutionException {
    // Number of threads that will try to persist the same Reservation object
    int threadCount = 10;

    // The Reservation object
    Reservation newReservation = buildReservation();

    // A task that persists the Reservation object
    Callable<Reservation> task = getCallable(newReservation);

    // A list of the task mentioned above
    List<Callable<Reservation>> tasks = Collections.nCopies(threadCount, task);

    // The thread pool that will execute all tasks from the list
    ExecutorService executorService = Executors.newFixedThreadPool(threadCount);

    // Here we are asking to execute all the tasks
    List<Future<Reservation>> futures = executorService.invokeAll(tasks);

    // This set will hold the persisted Reservation object
    HashSet<Reservation> reservationSet = new HashSet<Reservation>();

    // Here we will check for exceptions
    for (Future<Reservation> future : futures) {
      // Counts the number of threads that were executed
      threadExecutionCount.incrementAndGet();

      // If ReservationServiceImpl.save(Reservation reservation) method were not
      // synchronized, we would get a java.util.concurrent.ExecutionException
      // caused by a org.springframework.dao.DataIntegrityViolationException.
      //
      // The Reservation is stored only once at the database; future.get() returns the
      // same object.
      reservationSet.add(future.get());
    }

    // Since a Set does not stores duplicated objects, this set will contain only
    // one Reservation object.
    assertEquals(1, reservationSet.size());

    // Here we assure that all threads were executed
    assertEquals(threadExecutionCount.get(), threadCount);

    // There will be only one Reservation object in the database
    assertEquals(newReservation, reservationSet.iterator().next());
  }

  private Callable<Reservation> getCallable(Reservation newReservation) {
    Callable<Reservation> task = new Callable<Reservation>() {
      @Override
      public Reservation call() {
        return service.save(newReservation);
      }
    };

    return task;
  }

  private Reservation buildReservation() {
    Reservation newReservation = new Reservation();
    newReservation.setFromDate(LocalDate.parse("2019-12-01"));
    newReservation.setToDate(LocalDate.parse("2019-12-02"));

    Guest newGuest = new Guest();
    newGuest.setName("Dummy");
    newGuest.setEmail("dummy@email.com");
    newReservation.setGuest(newGuest);

    return newReservation;
  }
}
