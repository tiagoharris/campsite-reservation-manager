package com.upgrade.controller;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.hibernate.exception.ConstraintViolationException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.upgrade.CampsiteReservationManagerApplication;
import com.upgrade.entity.Guest;
import com.upgrade.entity.Reservation;
import com.upgrade.exception.ResourceNotFoundException;
import com.upgrade.exception.UnavailableDateRangeException;
import com.upgrade.service.ReservationService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = CampsiteReservationManagerApplication.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class ReservationControllerTest {

  @Autowired
  private MockMvc mvc;
  
  @MockBean
  private ReservationService reservationService;
  
  private LocalDate today = LocalDate.now();

  private LocalDate fiveDaysAheadFromToday = LocalDate.now().plusDays(5);
  
  private LocalDate tomorrow = LocalDate.now().plusDays(1);
  
  private LocalDate tomorrowPlusThreeDays = LocalDate.now().plusDays(3);
  
  private LocalDate tomorrowPlusOneMonth = tomorrow.plusMonths(1);
  
  private LocalDate tenDaysBeforeToday = LocalDate.now().minusDays(10);
  
  private LocalDate twoMonthsFromNow = LocalDate.now().plusMonths(2);
  
  private LocalDate twoMonthsFromNowPlusThreeDays = LocalDate.now().plusMonths(2).plusDays(3);
  
  private Reservation savedReservation = buildReservation();
  
  private UUID uuid = UUID.randomUUID();
  
  @Test
  public void whenFromDateIsNotNullAndToDateIsNull_thenReturnsError() throws Exception {
    mvc.perform(get("/api/reservation/availability?fromDate=" + tomorrow)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(missingDatesJsonErrorMessage())).andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenToDateIsNotNullAndFromDateIsNull_thenReturnsError() throws Exception {
    mvc.perform(get("/api/reservation/availability?toDate=" + tomorrow)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(missingDatesJsonErrorMessage())).andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenFromDateIsGreaterThanToDate_thenReturnsError() throws Exception {
    mvc.perform(get("/api/reservation/availability?fromDate=" + twoMonthsFromNow + "&toDate=" + tomorrow)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(fromDateGreaterThanToDateJsonErrorMessage())).andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenFromDateIsLessThanToday_thenReturnsError() throws Exception {
    mvc.perform(get("/api/reservation/availability?fromDate=" + tenDaysBeforeToday + "&toDate=" + tomorrow)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(fromDateLessThanTodayJsonErrorMessage())).andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenFromDateIsInInvalidFormat_thenReturnsError() throws Exception {
    mvc.perform(get("/api/reservation/availability?fromDate=invalidFormat&toDate=" + tomorrow)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(dateFieldIsInInvalidFormatJsonErrorMessage("fromDate", "invalidFormat"))).andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenToDateIsInInvalidFormat_thenReturnsError() throws Exception {
    mvc.perform(get("/api/reservation/availability?fromDate=" + tomorrow + "&toDate=invalidFormat")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(dateFieldIsInInvalidFormatJsonErrorMessage("toDate", "invalidFormat"))).andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenDatesAreNotInformed_thenDateRangeShouldBeDefaultedToOneMonth_andIfIsAvailable_thenReturnSuccess() throws Exception {
    Mockito.when(
        reservationService.findNextAvailableDates(tomorrow, tomorrowPlusOneMonth))
        .thenReturn(null);
    
    mvc.perform(get("/api/reservation/availability")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(dateRangeIsAvailableJsonMessage(tomorrow, tomorrowPlusOneMonth))).andExpect(status().is2xxSuccessful());
    
    verify(reservationService).findNextAvailableDates(tomorrow, tomorrowPlusOneMonth);
  }
  
  @Test
  public void whenDatesAreNotInformed_thenDateRangeShouldBeDefaultedToOneMonth_andIfIsNotAvailable_thenReturnAvailableDates() throws Exception {
    Mockito.when(
        reservationService.findNextAvailableDates(tomorrow, tomorrowPlusOneMonth))
        .thenReturn(buildNextAvailableDates());
    
    mvc.perform(get("/api/reservation/availability")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(dateRangeIsNotAvailableJsonErrorMessage(tomorrow, tomorrowPlusOneMonth))).andExpect(status().is2xxSuccessful());
    
    verify(reservationService).findNextAvailableDates(tomorrow, tomorrowPlusOneMonth);
  }

  @Test
  public void whenDateRangeIsValid_andIfIsAvailableForReservation_thenReturnSuccess() throws Exception {
    LocalDate fromDate = LocalDate.parse("2019-10-01");
    LocalDate toDate = LocalDate.parse("2019-10-03");
    
    Mockito.when(
        reservationService.findNextAvailableDates(fromDate, toDate))
        .thenReturn(null);
    
    mvc.perform(get("/api/reservation/availability?fromDate=" + fromDate + "&toDate=" + toDate)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(dateRangeIsAvailableJsonMessage(fromDate, toDate))).andExpect(status().is2xxSuccessful());
    
    verify(reservationService).findNextAvailableDates(fromDate, toDate);
  }
  
  @Test
  public void whenDateRangeIsValid_andIfIsNotAvailableForReservation_thenReturnAvailableDates() throws Exception {
    Mockito.when(
        reservationService.findNextAvailableDates(twoMonthsFromNow, twoMonthsFromNowPlusThreeDays))
        .thenReturn(buildNextAvailableDates());
    
    mvc.perform(get("/api/reservation/availability?fromDate=" + twoMonthsFromNow + "&toDate=" + twoMonthsFromNowPlusThreeDays)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(dateRangeIsNotAvailableJsonErrorMessage(twoMonthsFromNow, twoMonthsFromNowPlusThreeDays))).andExpect(status().is2xxSuccessful());
    
    verify(reservationService).findNextAvailableDates(twoMonthsFromNow, twoMonthsFromNowPlusThreeDays);
  }
  
  @Test
  public void whenPostWithoutBodyRequest_thenReturnError() throws Exception {
    mvc.perform(post("/api/reservation")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(missingRequestBody()))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenNamePropertyIsMissing_thenReturnError() throws Exception {
    mvc.perform(post("/api/reservation")
        .content(postReservationWithMissingNameJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(missingPropertyErrorString("name")))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenEmailPropertyIsMissing_thenReturnError() throws Exception {
    mvc.perform(post("/api/reservation")
        .content(postReservationWithMissingEmailJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(missingPropertyErrorString("email")))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenEmailPropertyIsInvalid_thenReturnError() throws Exception {
    mvc.perform(post("/api/reservation")
        .content(postReservationWithInvalidEmailJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(invalidEmailErrorString("email")))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenFromDatePropertyIsMissing_thenReturnError() throws Exception {
    mvc.perform(post("/api/reservation")
        .content(postReservationWithMissingFromDateJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(missingDatesPropertiesErrorString()))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenToDatePropertyIsMissing_thenReturnError() throws Exception {
    mvc.perform(post("/api/reservation")
        .content(postReservationWithMissingToDateJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(missingDatesPropertiesErrorString()))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenFromDateFormatIsInvalid_thenReturnError() throws Exception {
    mvc.perform(post("/api/reservation")
        .content(postReservationWithInvalidFromDateJsonString("invalid"))
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(dateFieldIsInInvalidFormatJsonProcessingErrorMessage("invalid")))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenToDateFormatIsInvalid_thenReturnError() throws Exception {
    mvc.perform(post("/api/reservation")
        .content(postReservationWithInvalidToDateJsonString("invalid"))
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(dateFieldIsInInvalidFormatJsonProcessingErrorMessage("invalid")))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenFromDateIsInThePast_thenReturnError() throws Exception {
    mvc.perform(post("/api/reservation")
        .content(postReservationWithFromDateInThePastJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(fromDateLessThanTodayJsonErrorMessage()))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenFromDateIsGreaterThanToDate_thenReturnError() throws Exception {
    mvc.perform(post("/api/reservation")
        .content(postReservationWithFromDateGreaterThanToDateJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(fromDateGreaterThanToDateJsonErrorMessage()))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenFromDateNotAheadThanToday_thenReturnError() throws Exception {
    mvc.perform(post("/api/reservation")
        .content(postReservationWithFromDateNotAheadThanTodayJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(fromDateNotAheadThanTodayJsonErrorMessage()))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenDateRangeExceedsMaxReservationPeriod_thenReturnError() throws Exception {
    mvc.perform(post("/api/reservation")
        .content(postReservationThatExceedsMaxReservationPeriodJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(reservationExceedsMaxReservationPeriodJsonErrorMessage()))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenDateRangeExceedsMaxMonthsAhead_thenReturnError() throws Exception {
    mvc.perform(post("/api/reservation")
        .content(postReservationThatExceedsMaxMonthsAheadJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(reservationExceedsMaxMonthsAheadJsonErrorMessage()))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenDateRangeIsValid_andIfIsAvailable_thenReturnSuccess() throws Exception {
    Mockito.when(
        reservationService.save(any()))
        .thenReturn(savedReservation);
    
    mvc.perform(post("/api/reservation")
        .content(postValidReservationJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(reservationIsDoneJsonMessage()))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void whenDateRangeIsValid_andIfIsAvailable_andUserAlreadyHaveAReservation_thenReturnError() throws Exception {
    Mockito.when(
        reservationService.save(any()))
        .thenThrow(ConstraintViolationException.class);
    
    mvc.perform(post("/api/reservation")
        .content(postValidReservationJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(userAlreadyHaveReservationJsonErrorMessage()))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenDateRangeIsValid_andIsNotAvailable_thenReturnError() throws Exception {
    UnavailableDateRangeException ex = new UnavailableDateRangeException(savedReservation.getFromDate(), savedReservation.getToDate());
    
    Mockito.when(
        reservationService.save(any()))
        .thenThrow(ex);
    
    mvc.perform(post("/api/reservation")
        .content(postValidReservationJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(periodNotAvailableJsonErrorMessage()))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenPutWithoutBodyRequest_thenReturnError() throws Exception {
    mvc.perform(put("/api/reservation/" + uuid)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(missingRequestBody()))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenPutWithoutUuidRequestParam_thenReturnError() throws Exception {
    mvc.perform(put("/api/reservation")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(missingRequestParam("PUT")))
        .andExpect(status().is5xxServerError());
  }
  
  @Test
  public void whenUpdatingInexistentReservation_thenReturnError() throws Exception {
    UUID inexistentUuid = UUID.randomUUID();
    ResourceNotFoundException ex = new ResourceNotFoundException(Reservation.class.getSimpleName(), "uuid", inexistentUuid);
    
    Mockito.when(
        reservationService.findByUUID(inexistentUuid))
        .thenThrow(ex);
    
    mvc.perform(put("/api/reservation/" + inexistentUuid)
        .content(putReservationJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(reservationNotFoundJsonErrorMessage(inexistentUuid)))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenUpdatingReservationWithMissingUuidProperty_thenReturnError() throws Exception {
    mvc.perform(put("/api/reservation/" + uuid)
        .content(putReservationWithoutUuidPropertyJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(missingPropertyErrorString("uuid")))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenUpdatingReservationToADanteRangeThatOverlapsExistingReservation_thenReturnError() throws Exception {
    UnavailableDateRangeException ex = new UnavailableDateRangeException(savedReservation.getFromDate(), savedReservation.getToDate());
    
    Mockito.when(
        reservationService.findByUUID(savedReservation.getUuid()))
        .thenReturn(savedReservation);
    
    Mockito.when(
        reservationService.save(any()))
        .thenThrow(ex);
    
    mvc.perform(put("/api/reservation/" + savedReservation.getUuid())
        .content(putReservationJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(periodNotAvailableJsonErrorMessage()))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenUpdatingReservationToADanteRangeThatDoNotOverlapsExistingReservation_thenReturnUpdatedReservation() throws Exception {
    Mockito.when(
        reservationService.findByUUID(savedReservation.getUuid()))
        .thenReturn(savedReservation);
    
    Mockito.when(
        reservationService.save(any()))
        .thenReturn(savedReservation);
    
    mvc.perform(put("/api/reservation/" + savedReservation.getUuid())
        .content(putReservationJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(reservationIsDoneJsonMessage()))
        .andExpect(status().is2xxSuccessful());
  }
  
  @Test
  public void whenDeleteWithoutUuidRequestParam_thenReturnError() throws Exception {
    mvc.perform(delete("/api/reservation")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(missingRequestParam("DELETE")))
        .andExpect(status().is5xxServerError());
  }
  
  @Test
  public void whenDeletingInexistentReservation_thenReturnError() throws Exception {
    ResourceNotFoundException ex = new ResourceNotFoundException(Reservation.class.getSimpleName(), "uuid", uuid);
    
    doThrow(ex).when(reservationService).delete(uuid);
    
    mvc.perform(delete("/api/reservation/" + uuid)
        .content(putReservationJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(reservationNotFoundJsonErrorMessage(uuid)))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenDeletingExistingReservation_thenReturnSuccess() throws Exception {
    Mockito.doNothing().when(reservationService).delete(uuid);
    
    mvc.perform(delete("/api/reservation/" + uuid)
        .content(putReservationJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is2xxSuccessful());
  }
  
  private String missingRequestParam(String httpMethodName) {
    return "{\"errorCode\":\"error\",\"errorMessage\":\"Request method '" + httpMethodName + "' not supported\"}";
  }
  
  private String missingRequestBody() {
    return "{\"errorCode\":\"Bad Request\",\"errorMessage\":\"Missing request body\"}";
  }
  
  private String missingDatesJsonErrorMessage() {
    return "{\"errorCode\":\"Bad Request\",\"errorMessage\":\"Invalid inputs\",\"errors\":[\"Please specify values for 'fromDate' and 'toDate' fields\"]}";
  }
  
  private String fromDateGreaterThanToDateJsonErrorMessage() {
    return "{\"errorCode\":\"Bad Request\",\"errorMessage\":\"Invalid inputs\",\"errors\":[\"'fromDate' cannot be greater than 'toDate'\"]}";
  }
  
  private String fromDateLessThanTodayJsonErrorMessage() {
    return "{\"errorCode\":\"Bad Request\",\"errorMessage\":\"Invalid inputs\",\"errors\":[\"'" + tenDaysBeforeToday + "' is past!\"]}";
  }
  
  private String dateFieldIsInInvalidFormatJsonErrorMessage(String dateNameField, String wrongValue) {
    return "{\"errorCode\":\"Bad Request\",\"errorMessage\":\"'" + dateNameField + "' should be a valid 'LocalDate' and '" + wrongValue + "' isn't\"}";
  }
  
  private String dateFieldIsInInvalidFormatJsonProcessingErrorMessage(String wrongValue) {
    return "{\"errorCode\":\"Bad Request\",\"errorMessage\":\"Text '" + wrongValue + "' could not be parsed at index 0\"}";
  }
  
  private String dateRangeIsAvailableJsonMessage(LocalDate fromDate, LocalDate toDate) {
    return "{\"fromDate\":\"" + fromDate +  "\",\"toDate\":\"" + toDate +  "\",\"available\":true}";
  }
  
  private String dateRangeIsNotAvailableJsonErrorMessage(LocalDate fromDate, LocalDate toDate) {
    String availableFromDate = twoMonthsFromNow.toString();
    String availableToDate = twoMonthsFromNowPlusThreeDays.toString();
    
    return "{\"fromDate\":\"" + fromDate +  "\",\"toDate\":\"" + toDate + "\",\"available\":false,\"availableDates\":[\"" + availableFromDate + "\",\"" + availableToDate + "\"]}";
  }
  
  private String postReservationWithMissingNameJsonString() {
    return "{\"fromDate\":\"" + tomorrow + "\", \"toDate\":\"" + tomorrowPlusThreeDays + "\", \"email\":\"email@email.com\"}";
  }
  
  private String postReservationWithMissingEmailJsonString() {
    return "{\"fromDate\":\"" + tomorrow + "\", \"toDate\":\"" + tomorrowPlusThreeDays + "\", \"name\":\"Tiago\"}";
  }
  
  private String postReservationWithInvalidEmailJsonString() {
    return "{\"fromDate\":\"" + tomorrow + "\", \"toDate\":\"" + tomorrowPlusThreeDays + "\", \"name\":\"Tiago\", \"email\":\"invalid\"}";
  }
  
  private String invalidEmailErrorString(String string) {
    return "{\"errorCode\":\"Bad Request\",\"errorMessage\":\"Invalid inputs\",\"errors\":[\"'email' must be a well-formed email address\"]}";
  }
  
  private String postReservationWithMissingFromDateJsonString() {
    return "{\"toDate\":\"" + tomorrowPlusThreeDays + "\", \"name\":\"Tiago\", \"email\":\"email@email.com\"}";
  }
  
  private String postReservationWithMissingToDateJsonString() {
    return "{\"fromDate\":\"" + tomorrowPlusThreeDays + "\", \"name\":\"Tiago\", \"email\":\"email@email.com\"}";
  }
  
  private String postReservationWithInvalidFromDateJsonString(String wrongValue) {
    return "{\"fromDate\":\"" + wrongValue + "\", \"toDate\":\"" + tomorrowPlusThreeDays + "\", \"name\":\"Tiago\", \"email\":\"email@email.com\"}";
  }
  
  private String postReservationWithInvalidToDateJsonString(String wrongValue) {
    return "{\"toDate\":\"" + wrongValue + "\", \"fromDate\":\"" + tomorrowPlusThreeDays + "\", \"name\":\"Tiago\", \"email\":\"email@email.com\"}";
  }
  
  private String postReservationWithFromDateInThePastJsonString() {
    return "{\"fromDate\":\"" + tenDaysBeforeToday + "\", \"toDate\":\"" + tomorrowPlusThreeDays + "\", \"name\":\"Tiago\", \"email\":\"email@email.com\"}";
  }
  
  private String postReservationWithFromDateGreaterThanToDateJsonString() {
    return "{\"fromDate\":\"" + twoMonthsFromNow + "\", \"toDate\":\"" + tomorrowPlusThreeDays + "\", \"name\":\"Tiago\", \"email\":\"email@email.com\"}";
  }

  private String postReservationWithFromDateNotAheadThanTodayJsonString() {
    return "{\"fromDate\":\"" + today + "\", \"toDate\":\"" + tomorrowPlusThreeDays + "\", \"name\":\"Tiago\", \"email\":\"email@email.com\"}";
  }
  
  private String fromDateNotAheadThanTodayJsonErrorMessage() {
    return "{\"errorCode\":\"Bad Request\",\"errorMessage\":\"Invalid inputs\",\"errors\":[\"the reservation should be done minimum 1 day(s) ahead of arrival\"]}";
  }

  private String postReservationThatExceedsMaxReservationPeriodJsonString() {
    return "{\"fromDate\":\"" + tomorrow + "\", \"toDate\":\"" + fiveDaysAheadFromToday + "\", \"name\":\"Tiago\", \"email\":\"email@email.com\"}";
  }
  
  private String reservationExceedsMaxReservationPeriodJsonErrorMessage() {
    return "{\"errorCode\":\"Bad Request\",\"errorMessage\":\"Invalid inputs\",\"errors\":[\"The period between '" + tomorrow + "' and '" + fiveDaysAheadFromToday + "' exceeds maximum allowed reservation period of 3 day(s)\"]}";
  }
  
  private String postReservationThatExceedsMaxMonthsAheadJsonString() {
    return "{\"fromDate\":\"" + twoMonthsFromNow + "\", \"toDate\":\"" + twoMonthsFromNowPlusThreeDays + "\", \"name\":\"Tiago\", \"email\":\"email@email.com\"}";
  }
  
  private String reservationExceedsMaxMonthsAheadJsonErrorMessage() {
    return "{\"errorCode\":\"Bad Request\",\"errorMessage\":\"Invalid inputs\",\"errors\":[\"The period between '" + twoMonthsFromNow + "' and '" + twoMonthsFromNowPlusThreeDays + "' is more than 1 moth(s) in advance\"]}";
  }

  private String postValidReservationJsonString() {
    Guest guest = savedReservation.getGuest();
    
    return "{\"fromDate\":\"" + savedReservation.getFromDate() + "\", \"toDate\":\"" + savedReservation.getToDate() + "\", \"name\":\"" + guest.getName() + "\", \"email\":\"" + guest.getEmail() + "\"}";
  }
  
  private String reservationIsDoneJsonMessage() {
    return "{\"fromDate\":\"" + savedReservation.getFromDate() + "\",\"toDate\":\"" + savedReservation.getToDate() + "\",\"uuid\":\"" + savedReservation.getUuid() + "\"}";
  }
  
  private String userAlreadyHaveReservationJsonErrorMessage() {
    return "{\"errorCode\":\"Conflict\",\"errorMessage\":\"This user already have a reservation\"}";
  }
  
  private String periodNotAvailableJsonErrorMessage() {
    return "{\"errorCode\":\"Conflict\",\"errorMessage\":\"The requested period between " + tomorrow + " and " + tomorrowPlusThreeDays + " is not available for reservation\"}";
  }
  
  private String putReservationJsonString() {
    Guest guest = savedReservation.getGuest();
    
    return "{\"uuid\": \"" + savedReservation.getUuid() + "\", \"fromDate\":\"" + savedReservation.getFromDate() + "\", \"toDate\":\"" + savedReservation.getToDate() + "\", \"name\":\"" + guest.getName() + "\", \"email\":\"" + guest.getEmail() + "\"}";
  }
  
  private String reservationNotFoundJsonErrorMessage(UUID inexistentUuid) {
    return "{\"errorCode\":\"Not Found\",\"errorMessage\":\"Reservation not found with uuid: '" + inexistentUuid + "'\"}";
  }
  
  private String putReservationWithoutUuidPropertyJsonString() {
    return "{\"fromDate\":\"" + savedReservation.getFromDate() + "\", \"toDate\":\"" + savedReservation.getToDate() + "\"}";
  }
  
  private String missingDatesPropertiesErrorString() {
    return "{\"errorCode\":\"Bad Request\",\"errorMessage\":\"Invalid inputs\",\"errors\":[\"Please specify values for 'fromDate' and 'toDate' fields\"]}";
  }
  
  private String missingPropertyErrorString(String propertyName) {
    return "{\"errorCode\":\"Bad Request\",\"errorMessage\":\"Invalid inputs\",\"errors\":[\"'" + propertyName + "' property is missing\"]}";
  }
  
  private List<LocalDate> buildNextAvailableDates() {
    List<LocalDate> nextAvailableDates = new ArrayList<LocalDate>();
    
    nextAvailableDates.add(twoMonthsFromNow);
    nextAvailableDates.add(twoMonthsFromNowPlusThreeDays);
    
    return nextAvailableDates;
  }
  
  private Reservation buildReservation() {
    Reservation reservation = new Reservation();
    Guest guest = new Guest();
    
    reservation.setFromDate(tomorrow);
    reservation.setToDate(tomorrowPlusThreeDays);
    reservation.setUuid(UUID.randomUUID());
    
    guest.setName("Tiago");
    guest.setEmail("email@email.com");

    reservation.setGuest(guest);
    
    return reservation;
  }
}
