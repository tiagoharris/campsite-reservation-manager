package com.upgrade.exception;

import static org.junit.Assert.assertEquals;

import org.hibernate.exception.ConstraintViolationException;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;

public class ExceptionHandlingControllerTest {

  private ExceptionHandlingController exceptionHandlingController = new ExceptionHandlingController(); 

  private DataIntegrityViolationException dataIntegrityViolationException = Mockito.mock(DataIntegrityViolationException.class);
  
  private HttpMessageNotReadableException httpMessageNotReadableException = Mockito.mock(HttpMessageNotReadableException.class);
  
  @Test
  public void whenTheresNoSpecificCause_thenReturnHttpMessageNotReadableExceptionMessage() {
    Mockito.when(
        httpMessageNotReadableException.getCause())
        .thenReturn(null);
    
    Mockito.when(
        httpMessageNotReadableException.getMessage())
        .thenReturn("message");
    
    ResponseEntity<ExceptionResponse> exceptionResponse = exceptionHandlingController.invalidRequestData(httpMessageNotReadableException);
    assertEquals("message", exceptionResponse.getBody().getErrorMessage());
  }
  
  @Test
  public void whenTheresNoSpecificCause_thenReturnDataIntegrityViolationExceptionMessage() {
    Mockito.when(
        dataIntegrityViolationException.getCause())
        .thenReturn(null);
    
    Mockito.when(
        dataIntegrityViolationException.getMessage())
        .thenReturn("message");
    
    ResponseEntity<ExceptionResponse> exceptionResponse = exceptionHandlingController.constraintViolation(dataIntegrityViolationException);
    assertEquals("message", exceptionResponse.getBody().getErrorMessage());
  }
  
  @Test
  public void whenTheresSpecificCause_andItsNotConstraintViolation_thenReturnDataIntegrityViolationExceptionMessage() {
    RuntimeException runtimeException = new RuntimeException("runtimeException message");
    
    Mockito.when(
        dataIntegrityViolationException.getCause())
        .thenReturn(runtimeException);
    
    Mockito.when(
        dataIntegrityViolationException.getMessage())
        .thenReturn("message");
    
    ResponseEntity<ExceptionResponse> exceptionResponse = exceptionHandlingController.constraintViolation(dataIntegrityViolationException);
    assertEquals("message", exceptionResponse.getBody().getErrorMessage());
  }
  
  @Test
  public void whenConstraintViolationException_thenReturnConstraintViolationExceptionMessage() {
    ConstraintViolationException cause = new ConstraintViolationException("", null, null);
    
    Mockito.when(
        dataIntegrityViolationException.getCause())
        .thenReturn(cause);
    
    ResponseEntity<ExceptionResponse> exceptionResponse = exceptionHandlingController.constraintViolation(dataIntegrityViolationException);
    assertEquals("This user already have a reservation", exceptionResponse.getBody().getErrorMessage());
  }
}
