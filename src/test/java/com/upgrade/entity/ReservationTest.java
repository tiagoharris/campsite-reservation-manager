package com.upgrade.entity;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.UUID;

import org.junit.Test;

public class ReservationTest {

  private UUID uuid = UUID.randomUUID();
  
  @Test
  public void whenTwoReservationObjectsHaveSameProperties_thenReturnsTrue() {
    LocalDate fromDate = LocalDate.parse("2019-01-01");
    LocalDate toDate = LocalDate.parse("2019-01-03");
    
    Reservation reservationOne = new Reservation();
    reservationOne.setFromDate(fromDate);
    reservationOne.setToDate(toDate);
    reservationOne.setUuid(uuid);
    
    Reservation reservationTwo = new Reservation();
    reservationTwo.setFromDate(fromDate);
    reservationTwo.setToDate(toDate);
    reservationTwo.setUuid(uuid);
    
    assertTrue(reservationOne.equals(reservationTwo));
    assertTrue(reservationOne.hashCode() == reservationTwo.hashCode());
  }
  
  @Test
  public void whenComparedToAnotherRandomObject_thenReturnsFalse() {
    assertFalse(new Reservation().equals(new Object()));
  }
}
