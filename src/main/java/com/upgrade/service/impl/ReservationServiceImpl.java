package com.upgrade.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.upgrade.entity.Reservation;
import com.upgrade.exception.ResourceNotFoundException;
import com.upgrade.exception.UnavailableDateRangeException;
import com.upgrade.repository.GuestRepository;
import com.upgrade.repository.ReservationRepository;
import com.upgrade.service.ReservationService;
import com.upgrade.util.DateUtil;

/**
 * Implements {@link ReservationService} interface.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@Service
public class ReservationServiceImpl implements ReservationService {

  @Autowired
  ReservationRepository reservationRepository;

  @Autowired
  GuestRepository guestRepository;

  @Autowired
  DateUtil dateUtils;
  
  /* (non-Javadoc)
   * @see com.upgrade.service.ReservationService#findNextAvailableDates(java.time.LocalDate, java.time.LocalDate)
   */
  @Override
  public List<LocalDate> findNextAvailableDates(LocalDate fromDate, LocalDate toDate) {
    List<LocalDate> nextAvailableDates = null;

    List<Object[]> reservations = reservationRepository.findAllReservationsBetweenMonths(fromDate.getMonthValue(), toDate.getMonthValue());

    boolean overlaps = dateUtils.checkIfDateRangeOverlaps(fromDate, toDate, reservations);

    if(overlaps) {
      nextAvailableDates = dateUtils.listAvailableDates(fromDate, toDate, reservations);
    }
    
    return nextAvailableDates;
  }
  
  /* (non-Javadoc)
   * @see com.upgrade.service.ReservationService#save(com.upgrade.entity.Reservation)
   */
  @Override
  public synchronized Reservation save(Reservation reservation) {
    Reservation overlappingReservation = reservationRepository.findOverlappingReservation(reservation.getFromDate(),
        reservation.getToDate());
    
    if (overlappingReservation != null) {
      if(! overlappingReservation.getId().equals(reservation.getId())) {
        throw new UnavailableDateRangeException(reservation.getFromDate(), reservation.getToDate()); 
      }
    } 
    
    if(reservation.getUuid() == null) {
      reservation.setUuid(UUID.randomUUID());
    }
    
    reservationRepository.save(reservation);
      
    return reservation;
  }

  /* (non-Javadoc)
   * @see com.upgrade.service.ReservationService#findByUUID(java.util.UUID)
   */
  @Override
  public Reservation findByUUID(UUID uuid) {
    Reservation reservation = reservationRepository.findByUuid(uuid).orElse(null);

    if (reservation == null) {
      throw new ResourceNotFoundException(Reservation.class.getSimpleName(), "uuid", uuid);
    }

    return reservation;
  }
  
  /* (non-Javadoc)
   * @see com.upgrade.service.ReservationService#delete(java.util.UUID)
   */
  @Override
  @Transactional
  public void delete(UUID uuid) {
    Reservation reservationToBeDeleted = findByUUID(uuid);
    
    reservationRepository.delete(reservationToBeDeleted);
    guestRepository.delete(reservationToBeDeleted.getGuest());
  }

}
