package com.upgrade.service;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import com.upgrade.entity.Reservation;
import com.upgrade.exception.ResourceNotFoundException;

/**
 * Service to manage reservations.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
public interface ReservationService {

  /**
   * If the desired date range overlaps a reservation, returns a list of
   * available dates.
   * 
   * @param fromDate
   * @param toDate
   * @return empty list if the desired date range is available
   */
  List<LocalDate> findNextAvailableDates(LocalDate fromDate, LocalDate toDate);

  /**
   * Saves a reservation
   * 
   * @param reservation
   * @return the saved reservation
   */
  Reservation save(Reservation reservation);
  
  /**
   * Finds a reservation by UUID.
   * 
   * @param uuid
   * @return the desired reservation
   * @throws ResourceNotFoundException if no reservation is found
   */
  Reservation findByUUID(UUID uuid);
  
  /**
   * Deletes a reservation for a given UUID.
   * 
   * @param uuid
   * @throws ResourceNotFoundException if no reservation is found
   */
  void delete(UUID uuid);
}
