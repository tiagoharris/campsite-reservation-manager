package com.upgrade.annotation.validator;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.PropertyAccessorFactory;

import com.upgrade.annotation.ValidateDateRangeReservation;

/**
 * Validator class used by {@link ValidateDateRangeReservation} annotation.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
public class ValidateDateRangeReservationValidator
    implements ConstraintValidator<ValidateDateRangeReservation, Object> {

  private String firstDateFieldName;

  private String secondDateFieldName;

  private LocalDate firstDateField;

  private LocalDate secondDateField;

  private int minDaysAheadOfArrivalDate;

  private int maxDaysOfReservation;

  private int maxMonthsInAdvance;

  private LocalDate today = LocalDate.now();

  private static final String NULL_FIELDS_MESSAGE_TEMPLATE = "Please specify values for '%s' and '%s\' fields";

  private static final String FROM_DATE_LESS_THAN_ACTUAL_DATE_ERROR_MESSAGE_TEMPLATE = "'%s' is past!";

  private static final String FROM_DATE_GREATER_THAN_TO_DATE_ERROR_MESSAGE_TEMPLATE = "'%s' cannot be greater than '%s'";

  private static final String MIN_DAYS_AHEAD_OF_ARRIVAL_ERROR_MESSAGE_TEMPLATE = "the reservation should be done minimum %s day(s) ahead of arrival";

  private static final String EXCEEDS_MAX_RESERVATION_PERIOD_ERROR_MESSAGE_TEMPLATE = "The period between '%s' and '%s' exceeds maximum allowed reservation period of %s day(s)";

  private static final String EXCEEDS_MAX_MONTHS_IN_ADVANCE_ERROR_MESSAGE_TEMPLATE = "The period between '%s' and '%s' is more than %s moth(s) in advance";

  @Override
  public void initialize(final ValidateDateRangeReservation constraintAnnotation) {
    firstDateFieldName = constraintAnnotation.firstDateField();
    secondDateFieldName = constraintAnnotation.secondDateField();
    minDaysAheadOfArrivalDate = constraintAnnotation.minDaysAheadOfArrivalDate();
    maxDaysOfReservation = constraintAnnotation.maxDaysOfReservation();
    maxMonthsInAdvance = constraintAnnotation.maxMonthsInAdvance();
  }

  @Override
  public boolean isValid(final Object value, ConstraintValidatorContext context) {
    BeanWrapper wrapper = PropertyAccessorFactory.forBeanPropertyAccess(value);

    firstDateField = (LocalDate) wrapper.getPropertyValue(firstDateFieldName);
    secondDateField = (LocalDate) wrapper.getPropertyValue(secondDateFieldName);

    if (firstDateField == null && secondDateField == null) {
      firstDateField = LocalDate.now().plusDays(1);
      secondDateField = firstDateField.plusMonths(1);

      wrapper.setPropertyValue(firstDateFieldName, firstDateField);
      wrapper.setPropertyValue(secondDateFieldName, secondDateField);
    }

    boolean bothFieldsAreNotNull = checkIfBothFieldsAreNotNull();

    if (!bothFieldsAreNotNull) {
      String message = String.format(NULL_FIELDS_MESSAGE_TEMPLATE, firstDateFieldName, secondDateFieldName);
      setValidationErrorMessage(context, message);
      return false;
    }

    boolean isInPresent = checkIfInPresent();

    if (!isInPresent) {
      String message = String.format(FROM_DATE_LESS_THAN_ACTUAL_DATE_ERROR_MESSAGE_TEMPLATE, firstDateField);
      setValidationErrorMessage(context, message);
      return false;
    }

    boolean isFromDateLessThanToDate = checkIfFromDateIsLessThanToDate();

    if (!isFromDateLessThanToDate) {
      String message = String.format(FROM_DATE_GREATER_THAN_TO_DATE_ERROR_MESSAGE_TEMPLATE, firstDateFieldName,
          secondDateFieldName);
      setValidationErrorMessage(context, message);
      return false;
    }

    boolean isAheadOfArrivalDate = checkIfAheadOfArrivalDate();

    if (!isAheadOfArrivalDate) {
      String message = String.format(MIN_DAYS_AHEAD_OF_ARRIVAL_ERROR_MESSAGE_TEMPLATE, minDaysAheadOfArrivalDate);
      setValidationErrorMessage(context, message);
      return false;
    }

    boolean isWithinMaxReservationPeriod = checkIfWithinMaxReservationPeriod();

    if (!isWithinMaxReservationPeriod) {
      String message = String.format(EXCEEDS_MAX_RESERVATION_PERIOD_ERROR_MESSAGE_TEMPLATE, firstDateField,
          secondDateField, maxDaysOfReservation);
      setValidationErrorMessage(context, message);
      return false;
    }

    boolean isWithinMaxMonthsInAdvance = checkIfWithinMaxMonthsInAdvance();

    if (!isWithinMaxMonthsInAdvance) {
      String message = String.format(EXCEEDS_MAX_MONTHS_IN_ADVANCE_ERROR_MESSAGE_TEMPLATE, firstDateField,
          secondDateField, maxMonthsInAdvance);
      setValidationErrorMessage(context, message);
      return false;
    }

    return true;
  }

  private boolean checkIfFromDateIsLessThanToDate() {
    if (firstDateField.isAfter(secondDateField)) {
      return false;
    }

    return true;
  }

  private boolean checkIfInPresent() {
    if (firstDateField.isBefore(today)) {
      return false;
    }

    return true;
  }

  private boolean checkIfAheadOfArrivalDate() {
    long daysBetween = ChronoUnit.DAYS.between(today, firstDateField);

    if (daysBetween < minDaysAheadOfArrivalDate) {
      return false;
    }

    return true;
  }

  private boolean checkIfBothFieldsAreNotNull() {
    if (firstDateField != null && secondDateField == null) {
      return false;
    } else if (firstDateField == null) {
      return false;
    }
    
    return true;
  }

  private boolean checkIfWithinMaxReservationPeriod() {
    long daysBetween = ChronoUnit.DAYS.between(firstDateField, secondDateField);

    if (maxDaysOfReservation > 0 && (daysBetween > maxDaysOfReservation)) {
      return false;
    }

    return true;
  }

  private boolean checkIfWithinMaxMonthsInAdvance() {
    long monthsBetween = ChronoUnit.MONTHS.between(today, firstDateField);

    if (maxMonthsInAdvance > 0 && (monthsBetween > maxMonthsInAdvance)) {
      return false;
    }

    return true;
  }

  private void setValidationErrorMessage(ConstraintValidatorContext context, String message) {
    context.disableDefaultConstraintViolation();
    context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
  }
}
