package com.upgrade.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.upgrade.annotation.validator.ValidateDateRangeReservationValidator;

/**
 * This annotation make all the necessary business validations on
 * a date range.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@Target({ ElementType.TYPE, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = { ValidateDateRangeReservationValidator.class })
public @interface ValidateDateRangeReservation {

  String message() default "";
  
  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
  
  /**
   * @return The first field
   */
  String firstDateField();

  /**
   * @return The second field
   */
  String secondDateField();
  
  int minDaysAheadOfArrivalDate() default 0;
  
  int maxDaysOfReservation() default 0;
  
  int maxMonthsInAdvance() default 0;
}
