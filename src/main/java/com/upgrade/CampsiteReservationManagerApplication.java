package com.upgrade;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * This is the main spring boot application class.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan
public class CampsiteReservationManagerApplication {

  public static void main(String[] args) {
    SpringApplication.run(CampsiteReservationManagerApplication.class, args);
  }

}
