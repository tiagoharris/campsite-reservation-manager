package com.upgrade.dto;

import java.time.LocalDate;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.upgrade.annotation.ValidateDateRangeReservation;
import com.upgrade.util.DateUtil;

/**
 * DTO used to check for the campsite's availability.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@ValidateDateRangeReservation(firstDateField = "fromDate", secondDateField = "toDate")
public class CampsiteAvailabilityDTO {

  @JsonFormat(pattern = DateUtil.DATE_PATTERN)
  @DateTimeFormat(pattern = DateUtil.DATE_PATTERN)
  private LocalDate fromDate;

  @JsonFormat(pattern = DateUtil.DATE_PATTERN)
  @DateTimeFormat(pattern = DateUtil.DATE_PATTERN)
  private LocalDate toDate;
  
  private boolean available;
  
  private List<LocalDate> availableDates;
  
  public LocalDate getFromDate() {
    return fromDate;
  }
  
  public void setFromDate(LocalDate fromDate) {
    this.fromDate = fromDate;
  }
  
  public LocalDate getToDate() {
    return toDate;
  }
  
  public void setToDate(LocalDate toDate) {
    this.toDate = toDate;
  }
  
  public boolean isAvailable() {
    available = (this.getAvailableDates() == null);
    
    return available;
  }
  
  public List<LocalDate> getAvailableDates() {
    return availableDates;
  }
  
  public void setAvailableDates(List<LocalDate> availableDates) {
    this.availableDates = availableDates;
  }
}
