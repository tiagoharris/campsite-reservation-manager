package com.upgrade.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * DTO used for reservation creation.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
public class PostReservationDTO extends ReservationDTO {

  @NotBlank(message = "'name' property is missing")
  private String name;
  
  @NotBlank(message = "'email' property is missing")
  @Email(message = "'email' must be a well-formed email address")
  private String email;
  
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }
}
