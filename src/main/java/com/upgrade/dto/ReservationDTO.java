package com.upgrade.dto;

import java.time.LocalDate;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.upgrade.annotation.ValidateDateRangeReservation;

/**
 * DTO with reservation information.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@ValidateDateRangeReservation(
    firstDateField = "fromDate", 
    secondDateField = "toDate",
    minDaysAheadOfArrivalDate = 1, 
    maxDaysOfReservation = 3, 
    maxMonthsInAdvance = 1)
public class ReservationDTO {

  @JsonIgnore
  private Integer id;
  
  private LocalDate fromDate;
  
  private LocalDate toDate;
  
  private UUID uuid;
  
  public void setId(Integer id) {
    this.id = id;
  }

  public LocalDate getFromDate() {
    return fromDate;
  }

  public void setFromDate(LocalDate fromDate) {
    this.fromDate = fromDate;
  }

  public LocalDate getToDate() {
    return toDate;
  }

  public void setToDate(LocalDate toDate) {
    this.toDate = toDate;
  }

  public UUID getUuid() {
    return uuid;
  }

  public void setUuid(UUID uuid) {
    this.uuid = uuid;
  }
}
