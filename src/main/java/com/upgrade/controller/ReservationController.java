package com.upgrade.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.upgrade.dto.CampsiteAvailabilityDTO;
import com.upgrade.dto.PostReservationDTO;
import com.upgrade.dto.ReservationDTO;
import com.upgrade.dto.UpdateReservationDTO;
import com.upgrade.entity.Guest;
import com.upgrade.entity.Reservation;
import com.upgrade.service.ReservationService;
import com.upgrade.util.DateUtil;

/**
 * Restful controller responsible for managing reservations.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@RestController
@RequestMapping("/api")
public class ReservationController {

  @Autowired
  ModelMapper modelMapper;

  @Autowired
  ReservationService reservationService;

  /**
   * Checks the campsite's availability for a given date range.
   * 
   * @param fromDate
   * @param toDate
   * @param campsiteAvailabilityDTO the object that encapsulates request data
   * @return availability information
   */
  @GetMapping("/reservation/availability")
  public CampsiteAvailabilityDTO getAvailability(
      @RequestParam(value = "fromDate", required = false) @DateTimeFormat(pattern = DateUtil.DATE_PATTERN) LocalDate fromDate,
      @RequestParam(value = "toDate", required = false) @DateTimeFormat(pattern = DateUtil.DATE_PATTERN) LocalDate toDate,
      @Valid CampsiteAvailabilityDTO campsiteAvailabilityDTO) {

    List<LocalDate> nextAvailableDates = reservationService
        .findNextAvailableDates(campsiteAvailabilityDTO.getFromDate(), campsiteAvailabilityDTO.getToDate());

    campsiteAvailabilityDTO.setAvailableDates(nextAvailableDates);

    return campsiteAvailabilityDTO;
  }

  /**
   * Creates a reservation.
   *  
   * @param postReservationDTO the object that encapsulates request data
   * @return reservation information
   */
  @PostMapping("/reservation")
  public ReservationDTO createReservation(
      @Valid @RequestBody PostReservationDTO postReservationDTO) {
    Reservation reservation = convertToReservationEntity(postReservationDTO);
    Guest guest = convertToGuestEntity(postReservationDTO);

    reservation.setGuest(guest);
    //guest.setReservation(reservation);
    
    return convertToReservationDTO(reservationService.save(reservation)); 
  }
  
  /**
   * Updates a reservation.
   * 
   * @param uuid the UUID of the desired reservation
   * @param updateReservationDTO encapsulates request data
   * @return the updated reservation
   */
  @PutMapping("/reservation/{uuid}")
  public ReservationDTO updateReservation(@PathVariable(value = "uuid") @NotNull UUID uuid, 
      @Valid @RequestBody UpdateReservationDTO updateReservationDTO) {
    updateReservationDTO.setUuid(uuid);
    Reservation reservation = convertToReservationEntity(updateReservationDTO);

    return convertToReservationDTO(reservationService.save(reservation));
  }
  
  /**
   * Deletes a reservation.
   * 
   * @param uuid the UUID of the desired reservation
   * @return
   */
  @DeleteMapping("/reservation/{uuid}")
  public ResponseEntity<?> cancelReservation(@PathVariable(value = "uuid") @NotNull UUID uuid) {
    reservationService.delete(uuid);

    return ResponseEntity.ok().build();
  }
  
  private Reservation convertToReservationEntity(ReservationDTO reservationDTO) {
    Reservation reservation = modelMapper.map(reservationDTO, Reservation.class);

    if (reservationDTO.getUuid() != null) {
      Reservation oldReservation = reservationService.findByUUID(reservationDTO.getUuid());
      reservation.setId(oldReservation.getId());
      reservation.setGuest(oldReservation.getGuest());
    }

    return reservation;
  }

  private Guest convertToGuestEntity(ReservationDTO campsiteNewReservationDTO) {
    return modelMapper.map(campsiteNewReservationDTO, Guest.class);
  }
  
  private ReservationDTO convertToReservationDTO(Reservation reservation) {
    return modelMapper.map(reservation, ReservationDTO.class);
  }
}
