package com.upgrade.util;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

/**
 * Utility class that deals with dates.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@Component
public class DateUtil {

  public static final String DATE_PATTERN = "yyyy-MM-dd";
  
  /**
   * Checks if a date range overlaps any dates from a list.
   * 
   * @param fromDate
   * @param toDate
   * @param reservations the List of dates
   * @return true if overlaps
   */
  public boolean checkIfDateRangeOverlaps(LocalDate fromDate, LocalDate toDate, List<Object[]> reservations) {
    boolean overlaps = false;
    
    for (Object[] date : reservations) {
      LocalDate fromDateDB = (LocalDate)date[0];
      LocalDate toDateDB = (LocalDate)date[1];

      if(! fromDateDB.isAfter(toDate) && ! toDateDB.isBefore(fromDate)) {
        overlaps = true;
        break;
      }
    }
    
    return overlaps;
  }
  
  /**
   * Lists the available dates from a given list of overlapping dates.
   * 
   * @param fromDate
   * @param toDate
   * @param overlappingDates the list of overlapping dates
   * @return the available dates
   */
  public List<LocalDate> listAvailableDates(LocalDate fromDate, LocalDate toDate, List<Object[]> overlappingDates) {
    List<LocalDate> unavailableDays = buildUnavailableDays(overlappingDates);
    
    List<LocalDate> allDates = buildAllDates(fromDate, toDate);
    
    return availableDates(allDates, unavailableDays);
  }

  /*
   * Utility method to build a sequential list of unavailable dates.
   */
  private List<LocalDate> buildUnavailableDays(List<Object[]> overlappingDates) {
    List<LocalDate> unavailableDays = new ArrayList<LocalDate>();
    
    for (Object[] date : overlappingDates) {
      LocalDate fromDate = (LocalDate)date[0];
      LocalDate toDate = (LocalDate)date[1];
      
      long daysBetween = ChronoUnit.DAYS.between(fromDate, toDate);
      
      LocalDate end = fromDate.plusDays(daysBetween);
      
      while(fromDate.isBefore(end)) {
        unavailableDays.add(fromDate);
        fromDate = fromDate.plusDays(1);
      }
      
      unavailableDays.add(toDate);
    }
    
    return unavailableDays;
  }
  
  /*
   * Utility method to build a sequential list comprehended between two dates.
   */
  private List<LocalDate> buildAllDates(LocalDate fromDate, LocalDate toDate) {
    List<LocalDate> allDates = new ArrayList<LocalDate>();
    
    LocalDate startDate = fromDate.withDayOfMonth(1);
    LocalDate endDate = toDate.plusMonths(1).withDayOfMonth(1).minusDays(1);
        
    while(! startDate.isAfter(endDate)) {
      allDates.add(startDate);
      startDate = startDate.plusDays(1);
    }
    
    return allDates;
  }
  
  /*
   * Utility method that makes a diff between two lists of dates.
   */
  private List<LocalDate> availableDates(List<LocalDate> datesFromCurrentMonth, List<LocalDate> unavailableDays) {
    List<LocalDate> differenceList = datesFromCurrentMonth.stream().filter(num -> ! unavailableDays.contains(num)).collect(Collectors.toList());

    return differenceList;
  }
}
