package com.upgrade.entity;

import java.time.LocalDate;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NaturalId;
import org.hibernate.annotations.NaturalIdCache;

/**
 * Entity for table "Reservation".
 * 
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@Entity(name = "reservation")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NaturalIdCache
public class Reservation {

  @Id
  private Integer id;

  @Column(nullable = false)
  private LocalDate fromDate;

  @Column(nullable = false)
  private LocalDate toDate;

  @NaturalId
  @Column(nullable = false, unique = true)
  private UUID uuid;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "guest_id")
  @MapsId
  private Guest guest;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    
    if (!(o instanceof Reservation)) {
      return false;
    }
    
    Reservation reservation = (Reservation) o;
    return Objects.equals(getUuid(), reservation.getUuid());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getUuid());
  }

  public LocalDate getFromDate() {
    return fromDate;
  }

  public void setFromDate(LocalDate fromDate) {
    this.fromDate = fromDate;
  }

  public LocalDate getToDate() {
    return toDate;
  }

  public void setToDate(LocalDate toDate) {
    this.toDate = toDate;
  }

  public UUID getUuid() {
    return uuid;
  }

  public void setUuid(UUID uuid) {
    this.uuid = uuid;
  }

  public Guest getGuest() {
    return guest;
  }

  public void setGuest(Guest guest) {
    this.guest = guest;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }
}
