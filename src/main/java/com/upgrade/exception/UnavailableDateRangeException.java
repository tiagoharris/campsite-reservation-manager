package com.upgrade.exception;

import java.time.LocalDate;

/**
 * Exception class that is thrown when a desired date range is not available.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
public class UnavailableDateRangeException extends RuntimeException {
  private static final long serialVersionUID = 1L;

  public UnavailableDateRangeException(LocalDate fromDate, LocalDate toDate) {
    super(String.format("The requested period between %s and %s is not available for reservation", fromDate, toDate));
  }
}
