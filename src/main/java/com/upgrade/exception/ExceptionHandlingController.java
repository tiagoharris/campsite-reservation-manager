package com.upgrade.exception;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.upgrade.util.ValidationUtil;

/**
 * This class handles the execptions thrown by the controller layer.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@ControllerAdvice
public class ExceptionHandlingController {

  @ExceptionHandler(ResourceNotFoundException.class)
  public ResponseEntity<ExceptionResponse> resourceNotFound(ResourceNotFoundException ex) {
    ExceptionResponse response = new ExceptionResponse();
    response.setErrorCode("Not Found");
    response.setErrorMessage(ex.getMessage());

    return new ResponseEntity<ExceptionResponse>(response, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ExceptionResponse> invalidInput(MethodArgumentNotValidException ex) {
    BindingResult result = ex.getBindingResult();
    ExceptionResponse response = new ExceptionResponse();
    response.setErrorCode("Bad Request");
    response.setErrorMessage("Invalid inputs");
    response.setErrors(new ValidationUtil().fromBindingErrors(result));
    return new ResponseEntity<ExceptionResponse>(response, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(BindException.class)
  public ResponseEntity<ExceptionResponse> invalidInput(BindException ex) {
    BindingResult result = ex.getBindingResult();
    ExceptionResponse response = new ExceptionResponse();
    response.setErrorCode("Bad Request");
    response.setErrorMessage("Invalid inputs");
    response.setErrors(new ValidationUtil().fromBindingErrors(result));
    return new ResponseEntity<ExceptionResponse>(response, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(HttpMessageNotReadableException.class)
  public ResponseEntity<ExceptionResponse> invalidRequestData(HttpMessageNotReadableException ex) {
    Throwable mostSpecificCause = ex.getMostSpecificCause();

    ExceptionResponse response = new ExceptionResponse();
    response.setErrorCode("Bad Request");

    if (mostSpecificCause != null) {
      String message = mostSpecificCause.getMessage();
      
      if(message.matches("(.*)Required request body is missing(.*)")) {
        response.setErrorMessage("Missing request body");
      } else {
        response.setErrorMessage(mostSpecificCause.getMessage());
      }
    } else {
      response.setErrorMessage(ex.getMessage());
    }

    return new ResponseEntity<ExceptionResponse>(response, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(MethodArgumentTypeMismatchException.class)
  public ResponseEntity<ExceptionResponse> handleTypeMismatch(MethodArgumentTypeMismatchException ex) {
    String name = ex.getName();
    String type = ex.getRequiredType().getSimpleName();
    Object value = ex.getValue();
    String message = String.format("'%s' should be a valid '%s' and '%s' isn't", name, type, value);

    ExceptionResponse response = new ExceptionResponse();
    response.setErrorCode("Bad Request");
    response.setErrorMessage(message);
    return new ResponseEntity<ExceptionResponse>(response, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(DataIntegrityViolationException.class)
  public ResponseEntity<ExceptionResponse> constraintViolation(DataIntegrityViolationException ex) {
    ExceptionResponse response = new ExceptionResponse();
    response.setErrorCode("Bad Request");

    final Throwable cause = ex.getCause();

    if (cause instanceof ConstraintViolationException) {
      response.setErrorMessage("This user already have a reservation");
    } else {
      response.setErrorMessage(ex.getMessage());
    }

    return new ResponseEntity<ExceptionResponse>(response, HttpStatus.BAD_REQUEST);
  }
  
  @ExceptionHandler(ConstraintViolationException.class)
  public ResponseEntity<ExceptionResponse> constraintViolation(ConstraintViolationException ex) {
    ExceptionResponse response = new ExceptionResponse();
    response.setErrorCode("Conflict");
    response.setErrorMessage("This user already have a reservation");

    return new ResponseEntity<ExceptionResponse>(response, HttpStatus.CONFLICT);
  }

  @ExceptionHandler(UnavailableDateRangeException.class)
  public ResponseEntity<ExceptionResponse> unavailableDateRange(UnavailableDateRangeException ex) {
    ExceptionResponse response = new ExceptionResponse();
    response.setErrorCode("Conflict");
    response.setErrorMessage(ex.getMessage());

    return new ResponseEntity<ExceptionResponse>(response, HttpStatus.CONFLICT);
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<ExceptionResponse> handleException(Exception ex) {
    ExceptionResponse response = new ExceptionResponse();
    response.setErrorCode("error");
    response.setErrorMessage(ex.getMessage());

    return new ResponseEntity<ExceptionResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
