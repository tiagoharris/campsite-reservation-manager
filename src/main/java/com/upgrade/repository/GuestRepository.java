package com.upgrade.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.upgrade.entity.Guest;

/**
 * Repository for {@link Guest} entity.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
public interface GuestRepository extends JpaRepository<Guest, Long>{ }
