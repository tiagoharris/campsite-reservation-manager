package com.upgrade.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.upgrade.entity.Reservation;

/**
 * Repository for {@link Reservation} entity.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long>{

  /**
   * List reservations comprehended between a date range.
   * 
   * @param fromMonth
   * @param toMonth
   * @return the list of dates
   */
  @Query("SELECT r.fromDate, r.toDate FROM reservation r WHERE MONTH(r.fromDate) BETWEEN ?1 AND ?2")
  List<Object[]> findAllReservationsBetweenMonths(int fromMonth, int toMonth);

  /**
   * List a reservation that is overlapped by a date range.
   * 
   * @param fromDate
   * @param toDate
   * @return null if no reservation is overlapped.
   */
  @Query("SELECT r FROM reservation r WHERE r.fromDate <= ?2 and r.toDate >= ?1")
  Reservation findOverlappingReservation(LocalDate fromDate, LocalDate toDate);
  
  /**
   * Finds a reservation by UUID.
   * 
   * @param uuid the UUID to be looked for
   * @return the reservation
   */
  Optional<Reservation> findByUuid(UUID uuid);
}
