CREATE TABLE IF NOT EXISTS `guest` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  
  PRIMARY KEY(`id`),
  UNIQUE(`name`, `email`)
) engine=InnoDB default charset=utf8;

CREATE TABLE IF NOT EXISTS `reservation` (
  `guest_id` int NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `uuid` uuid NOT NULL,
  
  PRIMARY KEY(`guest_id`)
  
) engine=InnoDB default charset=utf8;

CREATE UNIQUE INDEX IF NOT EXISTS index_reservation ON reservation
(
    `from_date`, `to_date`, `uuid`
);